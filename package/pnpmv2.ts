import {ModuleInfo,ModName} from './types.ts'
export * from './types.ts'

import Child from 'child_process'
import * as async from "../util/async.ts"

import Path from 'path'
import fs from 'fs'
import Os from 'os'
import crypto from 'crypto'
import url from 'url'

export class Registry{

	static executing = false 

	#workingFolder = ''
	#pnpmfolder = ''
	#pnpmbin = ''

	#r: Registry 

	$nostop = false 
	$useold = false 

	constructor(){
		
	}

	async getModuleInfoFromFolder(folder: string): Promise<ModuleInfo | null>{
        var pjson = Path.join(folder, "package.json")
        if(!fs.existsSync(pjson)){
            return null
        }
        
        var moduleinfo: ModuleInfo = {
            folder: '',
            main: '',
            name: '',
            version: '',
            packageJson: {},
            dependencies: []
        }
        moduleinfo.folder = folder
        moduleinfo.packageJson = require(pjson)
        moduleinfo.name = moduleinfo.packageJson?.name  || ''
        if (moduleinfo.packageJson?.dependencies) {
            for (var id in moduleinfo.packageJson.dependencies) {
                moduleinfo.dependencies.push({
                    name: id,
                    version: moduleinfo.packageJson.dependencies[id]
                })
            }
        }
        moduleinfo.version = moduleinfo.packageJson?.version || ''

		let main = ''
        if (moduleinfo.packageJson?.main) {
            main = Path.join(moduleinfo.folder, moduleinfo.packageJson.main)
        } else {
            main = Path.join(moduleinfo.folder, "index.js")
        }
		moduleinfo.main = main 
        return moduleinfo
    }

	async resolveSingle(mod: ModName){
		

		return await this.#resolveSingle(mod,'')
	}


	async resolve(modname: ModName | ModName[] | string){
		let ms = new Array<ModName>()
		if(typeof modname == "string"){
			let parts = modname.split("|")
			
			for(let part of parts){

				let name = '', path = '', version = ''
				let sparts = part.split("@")
				if(part[0] == "@"){
					name = "@" + sparts[1]
					let x = (sparts[2]||'').split("/")
					version = x.shift() || 'latest'
					path = x.join("/")
				}
				else{
					name = sparts[0]
					let x = (sparts[1]|| '').split("/")
					version = x.shift() || 'latest'
					path = x.join("/")
				}

				ms.push({
					name,
					path,
					version
				})
			}
		}

		else if(modname instanceof Array){
			ms = modname
		}
		else {
			ms = [modname]
		}
		
		let res = await this.resolveMany(ms)
	

		if(modname instanceof Array || res.length > 1){
			return res 
		}

		return res [0]
	}

	async require(){
		return this.secureRequire.apply(this, arguments)
	}

	async secureRequire(mod: ModName | ModName[] | string ){

		let res = await this.resolve(mod)
		let modinfo = (res instanceof Array) ? res[0] : res 
		if(!modinfo){
			throw new Error("Failed get module:" + String(mod))
		}


		let f = async  function(modinfo){

			if(modinfo.async){
				let g = require(modinfo.mainESM)
				return await g.load()
			}
			else{
				try{
					return require(modinfo.main)
				}catch(e){
					if((e.message.indexOf("REQUIRE_ESM") >= 0) || (e.message.indexOf("of ES Module") >= 0)){
						let g = require(modinfo.mainESM)
						return await g.load()
					}
					else{
						throw e
					}
				}
			}
		}

		try{
			return f(modinfo)	
		}catch(e){
			if(e.message.indexOf("Could not locate the bindings") >= 0){
                let folder = Path.join(modinfo.folder,"..","..")
				await this.executePnpm(folder, ["install", "--force", "--no-optional"])
				return f(modinfo)
            }
			else{
				throw e 
			}
		}
	}

	async resolveMany(ms: Array<ModName>){
		// obtener los datos ...
		let res = new Array<ModuleInfo | null>()
		for(let m of ms){
			let id = ''
			if(ms.length > 1){
				let uid = ms.map((a)=> `${a.name}--${a.version}`).join("|")
				id = crypto.createHash('sha1').update(uid).digest("hex")
			}
			res.push(await this.#resolveSingle(m, id, ms))
		}

		return res 
	}

	async #resolveSingle(mod: ModName, id: string, mods?: Array<ModName>){
		this.#initWorkingFolder()
		
		if(!mods){
			mods = [mod]
		}
		let sufix = ''
		if(mod.path){
			sufix = crypto.createHash('md5').update(mod.path).digest("hex")
		}
		let uname = mod.name.replace("/", "+") + "--" + mod.version
		if(id) uname = id  
		let ufolder = Path.join(this.#workingFolder, uname)

		let rname = mod.name.replace("/", "+") 
		if(rname[0] == "@") rname = rname.substring(1)
		let cfile = Path.join(ufolder, rname + sufix + ".js")
		let mfile = Path.join(ufolder, "loader.1.js")



		
		let m = Path.join(ufolder, "node_modules", mod.name)
		if(fs.existsSync(mfile)){
			let modinfo = await this.getModuleInfoFromFolder(m)
			if(modinfo){
				/*if(mod.path){
					modinfo.main = Path.join(modinfo.folder, mod.path)
				}*/
				modinfo.async = modinfo.main?.endsWith(".mjs") || (modinfo.packageJson?.type == 'module')

				modinfo.main = cfile 
				modinfo.mainESM = mfile
				return modinfo
			}
		}


		if(!fs.existsSync(ufolder)){
			await fs.promises.mkdir(ufolder)
		}

		let dep:any = {
			name: 'executer',
			dependencies: {
			}
		}
		for(let mio of mods){
			dep.dependencies[mio.name] = mio.version.replace("$","/")
		}


		await fs.promises.writeFile(Path.join(ufolder,"package.json"), JSON.stringify(dep))
		
		
		await fs.promises.writeFile(cfile, `
		module.exports = require(${JSON.stringify(Path.join(mod.name, mod.path || ''))})
		`)

		let res = await this.executePnpm(ufolder, ["i"])
		let modinfo = await this.getModuleInfoFromFolder(m)


		let p = mod.path ? Path.join(modinfo.folder, mod.path) : modinfo.main	
		let u = url.pathToFileURL(p)
		
		await fs.promises.writeFile(mfile, `
		var Path = require("path")
		module.exports.load = async function(name){
			if(name){
				name = Path.join(${JSON.stringify(modinfo.folder)}, name)
			}else{
				name = ${JSON.stringify(u.href)}
			}

			let mod= await import(name)
			if(mod){
				if(!mod.__esModule){
					mod.default = mod.default || mod
				}
			}
			return mod 
		}
		`)

		
		// execute PNPM 
		
		
		if(modinfo){
			
			modinfo.async = modinfo.main?.endsWith(".mjs") || (modinfo.packageJson?.type == 'module')
			modinfo .main = cfile 
			modinfo.mainESM = mfile
		}

		return modinfo
		

		
	}

	async #initWorkingFolder(){
		if(!this.#workingFolder){
			let folder = Path.join(Os.homedir(),".kawi")
			if(!fs.existsSync(folder)){
				await fs.promises.mkdir(folder)
			}
			folder = Path.join(folder,"user-data")
			if(!fs.existsSync(folder)){
				await fs.promises.mkdir(folder)
			}
			folder = Path.join(folder,"pnpm.service")
			if(!fs.existsSync(folder)){
				await fs.promises.mkdir(folder)
			}
			this.#workingFolder = folder 
		}

	}
	async executePnpm(folder: string, args: Array<string>){

		/*
		if(!this.$nostop){
			while(Registry.executing){
				await async.sleep(100)
			}
			Registry.executing = true 
		}*/

		try{
			return await this.#executePnpm(folder, args)
		}catch(e){
			throw e 
		}finally{
			if(this.$nostop) Registry.executing = false 
		}
	}
	async #executePnpm(folder: string, args: Array<string>){

		if(typeof fetch == "undefined"){
			globalThis.fetch = await import("https://esm.sh/fetch@1.1.0")	
		}

		this.#initWorkingFolder()
		let pnpmjs = Path.join(this.#workingFolder, "pnpm.js")
		if(!fs.existsSync(pnpmjs)){
			let res = await fetch("https://unpkg.com/pnpm@7.33.7/dist/pnpm.cjs")
			let text = await res.text()
			if(text.indexOf("use strict")>=0){
				// its ok 
				await fs.promises.writeFile(pnpmjs, text)
			}
			else{
				throw new Error("Failed load pnpm")
			}
		}
		

		// aquí ya Listo ..
		let v = Number(process.version.split(".")[0].substring(1))
		if(v >= 18 && !this.$useold){
		
			if(!this.#pnpmfolder){

				let pkg = await import("https://esm.sh/pnpm@9.x/package.json?time="+ Math.round(Date.now()/ (24*7*3600000)))
				console.info("> Installing/updating pnpm version:", pkg.version)
				let r = this.#r 
				if(!r){
					r = new Registry()
				}
				r.$nostop = true 
				r.$useold = true 
				this.#r = r 
				let desc:ModuleInfo = (await r.resolve("pnpm@" + pkg.version)) as ModuleInfo

				this.#pnpmbin = Path.join(desc.folder, desc.packageJson?.bin?.pnpm)
				this.#pnpmfolder = desc.folder || ''
			}

		}
		else{
			this.#pnpmfolder = this.#workingFolder
			this.#pnpmbin = pnpmjs
		}


		// ahora ya se puede ejecutar...
		let response = {
			stdout: new Array<Buffer>(),
			stderr: new Array<Buffer>(),
			stdoutText: '',
			stderrText: ''
		}
		
		let def = new async.Deferred<void>()
		console.info("> Executing:", folder, process.execPath, this.#pnpmbin, args)
		let p = Child.spawn(process.execPath, [this.#pnpmbin, ...args],{
			cwd: folder, 
			env:Object.assign({}, process.env, { RUN_AS_NODE: "1", ELECTRON_RUN_AS_NODE: "1", NODE_ENV:"production"})
		})

		// workaround
		p.stdin.write("y\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\n")

		p.stdout.on("data", function(bytes){
			response.stdout .push(bytes)
			process.stdout.write(bytes)
		})
		p.stderr.on("data", function(bytes){
			response.stderr .push(bytes)
			process.stderr.write(bytes)
		})
		p.on("error", def.reject)
		p.on("exit", def.resolve)
		await def.promise

		response.stdoutText = Buffer.concat(response.stdout).toString()
		response.stderrText = Buffer.concat(response.stderr).toString()
		
		return response

	}




}
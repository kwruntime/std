import {ModuleInfo,ModName} from './types.ts'
export * from './types.ts'

import Child from 'child_process'
import * as async from "../util/async.ts"

import Path from 'path'
import fs from 'fs'
import Os from 'os'
import crypto from 'crypto'

import { Registry as PRegistry } from './pnpmv2.ts'


export class Registry{

	static executing = false 

	#workingFolder = ''
	#folder = ''
	#bin = ''

	#r: PRegistry 

	$nostop = false 
	$useold = false 

	constructor(){
		
	}

	async getModuleInfoFromFolder(folder: string): Promise<ModuleInfo | null>{
        var pjson = Path.join(folder, "package.json")
        if(!fs.existsSync(pjson)){
            return null
        }
        
        var moduleinfo: ModuleInfo = {
            folder: '',
            main: '',
            name: '',
            version: '',
            packageJson: {},
            dependencies: []
        }
        moduleinfo.folder = folder
        moduleinfo.packageJson = require(pjson)
        moduleinfo.name = moduleinfo.packageJson?.name  || ''
        if (moduleinfo.packageJson?.dependencies) {
            for (var id in moduleinfo.packageJson.dependencies) {
                moduleinfo.dependencies.push({
                    name: id,
                    version: moduleinfo.packageJson.dependencies[id]
                })
            }
        }
        moduleinfo.version = moduleinfo.packageJson?.version || ''
        if (moduleinfo.packageJson?.main) {
            moduleinfo.main = Path.join(moduleinfo.folder, moduleinfo.packageJson.main)
        } else {
            moduleinfo.main = Path.join(moduleinfo.folder, "index.js")
        }
        return moduleinfo
    }

	async resolveSingle(mod: ModName){
		return await this.#resolveSingle(mod,'')
	}


	async resolve(modname: ModName | ModName[] | string){
		let ms = new Array<ModName>()
		if(typeof modname == "string"){
			let parts = modname.split("|")
			
			for(let part of parts){

				let name = '', path = '', version = ''
				let sparts = part.split("@")
				if(part[0] == "@"){
					name = "@" + sparts[1]
					let x = (sparts[2]||'').split("/")
					version = x.shift() || 'latest'
					path = x.join("/")
				}
				else{
					name = sparts[0]
					let x = (sparts[1]|| '').split("/")
					version = x.shift() || 'latest'
					path = x.join("/")
				}

				ms.push({
					name,
					path,
					version
				})
			}
		}

		else if(modname instanceof Array){
			ms = modname
		}
		else {
			ms = [modname]
		}

		let res = await this.resolveMany(ms)
	

		if(modname instanceof Array || res.length > 1){
			return res 
		}

		return res [0]
	}


	async require(mod: ModName | ModName[] | string ){

		let res = await this.resolve(mod)
		let modinfo = (res instanceof Array) ? res[0] : res 
		if(!modinfo){
			throw new Error("Failed get module:" + String(mod))
		}

		let f = async  function(modinfo){
			let g = require(modinfo.main)
			if(modinfo.async){
				return await g.load()
			}
			else{
				return g 
			}
		}

		try{
			return f(modinfo)	
		}catch(e){
			if(e.message.indexOf("Could not locate the bindings") >= 0){
                let folder = Path.join(modinfo.folder,"..","..")
				await this.executeYarn(folder, ["install", "--force"])
				return f(modinfo)
            }
			else{
				throw e 
			}
		}
	}

	async resolveMany(ms: Array<ModName>){
		// obtener los datos ...
		let res = new Array<ModuleInfo | null>()
		for(let m of ms){
			let id = ''
			if(ms.length > 1){
				let uid = ms.map((a)=> `${a.name}--${a.version}`).join("|")
				id = crypto.createHash('sha1').update(uid).digest("hex")
			}
			res.push(await this.#resolveSingle(m, id, ms))
		}

		return res 
	}

	async #resolveSingle(mod: ModName, id: string, mods?: Array<ModName>){
		this.#initWorkingFolder()

		if(!mods){
			mods = [mod]
		}
		let sufix = ''
		if(mod.path){
			sufix = crypto.createHash('md5').update(mod.path).digest("hex")
		}
		let uname = mod.name.replace("/", "+") + "--" + mod.version
		if(id) uname = id  
		let ufolder = Path.join(this.#workingFolder, uname)


		let cfile = Path.join(ufolder, mod.name + sufix + ".js")
		let mfile = Path.join(ufolder, mod.name + sufix + ".m.js")



		
		let m = Path.join(ufolder, "node_modules", mod.name)
		if(fs.existsSync(m)){
			let modinfo = await this.getModuleInfoFromFolder(m)
			if(modinfo){
				/*if(mod.path){
					modinfo.main = Path.join(modinfo.folder, mod.path)
				}*/
				modinfo.async = modinfo.main?.endsWith(".mjs")

				modinfo.main = cfile 
				modinfo.mainESM = mfile
				return modinfo
			}
		}


		if(!fs.existsSync(ufolder)){
			await fs.promises.mkdir(ufolder)
		}

		let dep:any = {
			name: 'executer',
			dependencies: {
			}
		}
		for(let mio of mods){
			dep.dependencies[mio.name] = mio.version
		}


		await fs.promises.writeFile(Path.join(ufolder,"package.json"), JSON.stringify(dep))
		

		await fs.promises.writeFile(cfile, `
		module.exports = require(${JSON.stringify(Path.join(mod.name, mod.path || ''))})
		`)

		await fs.promises.writeFile(mfile, `
		module.exports.load = async function(){
			return await import(${JSON.stringify(Path.join(mod.name, mod.path || ''))})
		}
		`)

		
		// execute 
		let res = await this.executeYarn(ufolder, [])
		let modinfo = await this.getModuleInfoFromFolder(m)
		if(modinfo){
			
			modinfo.async = modinfo.main?.endsWith(".mjs")
			modinfo .main = cfile 
			modinfo.mainESM = mfile
		}

		return modinfo
		

		
	}

	async #initWorkingFolder(){
		if(!this.#workingFolder){
			let folder = Path.join(Os.homedir(),".kawi")
			if(!fs.existsSync(folder)){
				await fs.promises.mkdir(folder)
			}
			folder = Path.join(folder,"user-data")
			if(!fs.existsSync(folder)){
				await fs.promises.mkdir(folder)
			}
			folder = Path.join(folder,"yarn.service")
			if(!fs.existsSync(folder)){
				await fs.promises.mkdir(folder)
			}
			this.#workingFolder = folder 
		}

	}
	async executeYarn(folder: string, args: Array<string>){

		if(!this.$nostop){
			while(Registry.executing){
				await async.sleep(100)
			}
		
			Registry.executing = true 
		}
		try{
			return await this.#executeYarn(folder, args)
		}catch(e){
			throw e 
		}finally{
			if(this.$nostop) Registry.executing = false 
		}
	}
	async #executeYarn(folder: string, args: Array<string>){

		if(typeof fetch == "undefined"){
			globalThis.fetch = await import("https://esm.sh/fetch@1.1.0")	
		}

		if(!this.#r){
			this.#r = new PRegistry()
			this.#r.$useold = true 
		}

		if(!this.#bin){
			let pkg = await import("https://unpkg.com/yarn@latest/package.json?time="+ Math.round(Date.now()/ (24*7*3600000)))
			let modinfo = await this.#r.resolve("yarn@" + pkg.version) as ModuleInfo

			this.#folder = modinfo.folder || ''
			this.#bin = Path.join(modinfo.folder, modinfo.packageJson?.bin?.yarn)

		}
		
		// ahora ya se puede ejecutar...
		let response = {
			stdout: new Array<Buffer>(),
			stderr: new Array<Buffer>(),
			stdoutText: '',
			stderrText: ''
		}
		
		let def = new async.Deferred<void>()
		//console.info("> Executing:", process.execPath, this.#bin, args)
		let p = Child.spawn(process.execPath, [this.#bin, ...args],{
			cwd: folder, 
			env:Object.assign({}, process.env, { RUN_AS_NODE: "1", ELECTRON_RUN_AS_NODE: "1", NODE_ENV:"production"})
		})

		p.stdout.on("data", function(bytes){
			response.stdout .push(bytes)
			process.stdout.write(bytes)
		})
		p.stderr.on("data", function(bytes){
			response.stderr .push(bytes)
			process.stderr.write(bytes)
		})
		p.on("error", def.reject)
		p.on("exit", def.resolve)
		await def.promise

		response.stdoutText = Buffer.concat(response.stdout).toString()
		response.stderrText = Buffer.concat(response.stderr).toString()
		
		return response

	}




}
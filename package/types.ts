
export interface ModuleInfo{
    name: string,
    version: string,
    main?: string,
    mainESM?: string 
    async?: boolean
    folder?: string,
    packageJson?: PackageJsonInfo,
    dependencies?: ModuleInfo[]
}

export interface PackageJsonInfo{
    name?: string,
    version?: string, 
    dependencies?: any,
    main?: string
    bin?: any 
}

export interface ModName{
    name: string
    version?: string 
    path?: string
}

